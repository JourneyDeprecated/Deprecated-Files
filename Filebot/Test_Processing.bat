@ECHO OFF

:: Media file processing script for Filebot
:: This script gets triggered by some means (inotify, auditd, cron, etc.) and processes media files in some locations.
:: It will run the filebot amc.groovy script against these files and output them into user defined locations.
:: CLI Stuff http://www.filebot.net/cli.html

:: Qbittorrent Call ---
:: "C:\Users\JourneyOver\Dropbox\Public\Folders\Filebot\MyStuff\Filebot_Process.bat" %N %F

:: Set up some logging
set LOGDIR=C:/Users/JourneyOver/Dropbox/Public/Folders/Filebot/logs

:: Base destinations to move files, no trailing slash. The subfolder is defined in the format string.
set DESTINATION=D:/Shows

:: Passing Args from Qbittorrent
set TORRENT_NAME="%1"
set TORRENT_PATH="%2"
::set TORRENT_LABEL="%3"

:: Rename action i.e: move | copy | keeplink | symlink | hardlink
set ACTION=move

:: Conflict resolution i.e: override | skip | fail
set CONFLICT=override

:: Episode filtering
set FILTERPATH=@/C:/Users/JourneyOver/Dropbox/Public/Folders/Filebot/MyStuff/filter.txt

:: Process music files
set MUSIC=false

:: Download subtitles for the given languages i.e: en | de | fr
set SUBTITLES=

:: Fetch artwork/nfo
set ARTWORK=false

:: Generate *.url files and fetch all available backdrops
set EXTRAS=false

:: Tell the given Kodi/XBMC instance to rescan it's library
::Set KODI=host[:port]

:: Tell the given Plex instance to rescan it's library. Plex Home instances require an authentication token
::set PLEX=localhost

:: Tell the given Emby instance to rescan it's library
::set EMBY=host:apikey

:: Save reports to local filesystem
set STOREREPORT=false

:: Do not extract archives
set SKIPEXTRACT=false

:: Automatically remove empty folders and clutter files that may be left behind after moving the video files, or temporary extracted files after copying
set CLEAN=true

:: Delete archives after extraction
set DELETEAFTEREXTRACT=true

:: Format path and strings for filebot, please see: http://www.filebot.net/naming.html
set FORMATPATH=@/C:/Users/JourneyOver/Dropbox/Public/Folders/Filebot/MyStuff/Formatting.txt

:: Run filebot script
filebot -script fn:amc ^
					--output "%DESTINATION%" ^
					--log-file "%LOGDIR%/filebot-amc.log" ^
					--action %ACTION% ^
					--conflict %CONFLICT% ^
					-non-strict ^
					-no-xattr ^
					--filter ^
					%FILTERPATH% ^
					--def ^
					ut_dir=%TORRENT_PATH% ^
					ut_kind=multi ^
					ut_title=%TORRENT_NAME% ^
					excludeList="%LOGDIR%/filebot-history.log" ^
					music=%MUSIC% ^
					subtitles=%SUBTITLES% ^
					artwork=%ARTWORK% ^
					extras=%EXTRAS% ^
					storeReport=%STOREREPORT% ^
					skipExtract=%SKIPEXTRACT% ^
					clean=%CLEAN% ^
					deleteAfterExtract=%DELETEAFTEREXTRACT% ^
					--def ^
					%FORMATPATH%