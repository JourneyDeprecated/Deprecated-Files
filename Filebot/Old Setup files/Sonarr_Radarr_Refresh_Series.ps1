$url = "http://localhost:8989/api/command"
$json = "{ ""name"": ""RescanSeries"" }"

# $url2 = "http://localhost:7878/api/command"
# $json2 = "{ ""name"": ""RescanMovie"" }"

Write-Host "Publishing update $version ($branch) to: $url"
Invoke-RestMethod -Uri $url -Method Post -Body $json -Headers @{"X-Api-Key"="APIKEY"}

# Start-Sleep -Milliseconds 60000

# Write-Host "Publishing update $version ($branch) to: $url2"
# Invoke-RestMethod -Uri $url2 -Method Post -Body $json2 -Headers @{"X-Api-Key"="APIKEY"}