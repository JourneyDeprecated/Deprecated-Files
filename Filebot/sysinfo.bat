@echo off

:: Set up some logging
set LOGDIR=C:/Users/JourneyOver/Dropbox/Public/Folders/Filebot/logs

filebot -script fn:sysinfo ^
--log-file "%LOGDIR%/filebot-sysinfo.log"